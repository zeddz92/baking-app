package com.fangio.edwardr.bakingapp.activities;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;

import com.fangio.edwardr.bakingapp.Mocks.FakeRecipe;
import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.models.Recipe;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by edwardr on 12/13/17.
 */

public class StepDetailActivityTest {

    private  static final String INTENT_RECIPE_STEP_POSITION = "com.fangio.edwardr.bakingapp.intent.recipe.step.position";
    @Rule
    public ActivityTestRule<StepDetailActivity> mActivityTestRule = new ActivityTestRule<StepDetailActivity>(StepDetailActivity.class) {

        @Override
        protected Intent getActivityIntent() {
            Intent intent = new Intent();
            Recipe recipe = FakeRecipe.getRecipe();
            intent.putExtra(RecipeDetailActivityTest.INTENT_RECIPE_KEY, recipe);
            intent.putExtra(INTENT_RECIPE_STEP_POSITION, 1);
            return intent;
        }
    };

    @Test
    public void clickStepWithNoVideo_showsVideoError() {
        onView(withId(R.id.text_view_player_error)).check(matches(isDisplayed()));
    }


    @Test
    public void clickNextButton_showsNextStep() {
        onView(withId(R.id.button_next_step)).perform(click());

        int newPos = 2;
        onView(withId(R.id.text_view_short_description)).check(matches(withText(FakeRecipe.getFakeSteps().get(newPos).getShortDescription())));

    }

    @Test
    public void clickPrevButton_showsPrevStep() {
        onView(withId(R.id.button_prev_step)).perform(click());

        int newPos = 0;
        onView(withId(R.id.text_view_short_description)).check(matches(withText(FakeRecipe.getFakeSteps().get(newPos).getShortDescription())));

    }




}
