package com.fangio.edwardr.bakingapp.activities;

import android.content.Intent;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.fangio.edwardr.bakingapp.Mocks.FakeRecipe;

import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.models.Recipe;

import org.hamcrest.Matchers;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.fangio.edwardr.bakingapp.utils.TestUtils.childAtPosition;


/**
 * Created by edwardr on 12/13/17.
 */
@RunWith(AndroidJUnit4.class)
public class RecipeDetailActivityTest {
    public static final String INTENT_RECIPE_KEY = "com.fangio.edwardr.bakingapp.intent.recipe";
    private static final String INGREDIENTS_TEXT = "Ingredients";


    @Rule
    public ActivityTestRule<RecipeDetailActivity> mActivityTestRule = new ActivityTestRule<RecipeDetailActivity>(RecipeDetailActivity.class) {

        @Override
        protected Intent getActivityIntent() {
            Intent intent = new Intent();
            Recipe recipe = FakeRecipe.getRecipe();
            intent.putExtra(INTENT_RECIPE_KEY, recipe);
            return intent;
        }
    };


    @Test
    public void clickIngredientsItem_opensIngredientActivity() {

        int position = 0;

        onView(withId(R.id.content_steps_recycler))
                .perform(RecyclerViewActions.actionOnItemAtPosition(position, click()));

        String ingredientText = FakeRecipe.getFakeIngredients()
                .get(position)
                .getIngredient();

        onView(
                Matchers.allOf(withId(R.id.text_view_ingredient_name), withText(ingredientText),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        1),
                                position),
                        isDisplayed()))
                .check(matches(withText(ingredientText)));
    }

    @Test
    public void clickStepItem_opensStepDetailActivity() {

        int position = 1;

        onView(withId(R.id.content_steps_recycler))
                .perform(RecyclerViewActions.actionOnItemAtPosition(position, click()));

        //Substract 1 to skip the ingredients item
        onView(withId(R.id.text_view_short_description)).check(matches(withText(FakeRecipe.getFakeSteps().get(position - 1).getShortDescription())));


    }


}
