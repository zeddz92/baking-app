package com.fangio.edwardr.bakingapp.interfaces;

/**
 * Created by edwardr on 12/10/17.
 */

public interface PlayerModeCallback<T> {
    void setPLayerView(T binding, int orientation);
}
