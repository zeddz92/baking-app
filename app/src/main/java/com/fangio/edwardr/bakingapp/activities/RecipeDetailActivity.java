package com.fangio.edwardr.bakingapp.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.databinding.FragmentStepDetailBinding;
import com.fangio.edwardr.bakingapp.fragments.StepDetailFragment;
import com.fangio.edwardr.bakingapp.fragments.StepListFragment;
import com.fangio.edwardr.bakingapp.interfaces.PlayerModeCallback;
import com.fangio.edwardr.bakingapp.interfaces.OnItemClickListener;
import com.fangio.edwardr.bakingapp.interfaces.OnNextPrevStep;
import com.fangio.edwardr.bakingapp.models.Recipe;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;

public class RecipeDetailActivity extends AppCompatActivity implements OnItemClickListener, OnNextPrevStep, PlayerModeCallback {
    private Recipe mRecipe;
    private boolean mTabletSize = false;
    private int currentStepPosition = 0;
    private FrameLayout frameLayoutStepList;
    private View divider;
    private boolean fullScreenMode = false;
    private ActionBar actionBar;


    private void setStepDetailFragment(int stepPosition) {
        StepDetailFragment stepDetailFragment = StepDetailFragment.newInstance(mRecipe, stepPosition);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.step_detail_container, stepDetailFragment).commit();

        if (mTabletSize) {
            int currentOrientation = getResources().getConfiguration().orientation;
            setPlayerMode(currentOrientation, stepDetailFragment.getBinding());
        }
    }


    private void setPlayerMode(int orientation, FragmentStepDetailBinding binding) {
        if (binding != null) {
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                frameLayoutStepList.setVisibility(View.GONE);
                divider.setVisibility(View.GONE);
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) binding.playerView.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                binding.playerView.setLayoutParams(params);

                if (actionBar != null) {
                    actionBar.hide();
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE);
                }
            } else {
                frameLayoutStepList.setVisibility(View.VISIBLE);
                divider.setVisibility(View.VISIBLE);
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) binding.playerView.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = (int) getResources().getDimension(R.dimen.exo_player_params_height);
                binding.playerView.setLayoutParams(params);

                if (actionBar != null) {
                    actionBar.show();
                }

            }
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(getString(R.string.instance_full_screen_key), fullScreenMode);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);
        mTabletSize = getResources().getBoolean(R.bool.isTablet);
        actionBar = getSupportActionBar();
        frameLayoutStepList = findViewById(R.id.step_list_container);
        divider = findViewById(R.id.view_divider);
        Intent intentThatStartedThisActivity = getIntent();

        if (intentThatStartedThisActivity.hasExtra(getString(R.string.intent_recipe_key))) {
            mRecipe = intentThatStartedThisActivity.getParcelableExtra(getString(R.string.intent_recipe_key));
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setTitle(mRecipe.getName());
            }
        }

        if (mTabletSize) {
            if (savedInstanceState == null) {
                setStepDetailFragment(0);
            }
        }

        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .add(R.id.step_list_container, StepListFragment.newInstance(mRecipe)).commit();
        }
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.containsKey(getString(R.string.instance_full_screen_key))) {
            fullScreenMode = savedInstanceState.getBoolean(getString(R.string.instance_full_screen_key));
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(int clickedItemIndex) {
        final String INGREDIENTS_KEY = getString(R.string.intent_recipe_ingredients_key);
        currentStepPosition = clickedItemIndex;

        if (clickedItemIndex > 0) {
            if (mTabletSize) {
                setStepDetailFragment(clickedItemIndex);
            } else {
                Intent intent = new Intent(this, StepDetailActivity.class);
                intent.putExtra(getString(R.string.intent_recipe_steps_key), mRecipe.getSteps());
                intent.putExtra(getString(R.string.intent_recipe_key), mRecipe);
                intent.putExtra(getString(R.string.intent_recipe_step_position_key), clickedItemIndex);
                startActivity(intent);
            }
        } else {
            Intent intent = new Intent(this, IngredientActivity.class);
            intent.putExtra(INGREDIENTS_KEY, mRecipe.getIngredients());
            startActivity(intent);
        }
    }


    @Override
    public void onNextButtonClick() {
        int newPos = currentStepPosition + 1;
        if (newPos < mRecipe.getSteps().size()) {
            setStepDetailFragment(newPos);
            currentStepPosition++;
        }
    }


    @Override
    public void onPrevButtonClick() {
        int newPos = currentStepPosition - 1;
        if (newPos > 0) {
            setStepDetailFragment(newPos);
            currentStepPosition--;
        }
    }


    @Override
    public void onFullScreenButtonClick(Object dataBinding) {
        FragmentStepDetailBinding binding = (FragmentStepDetailBinding) dataBinding;
        fullScreenMode = !fullScreenMode;

        if (fullScreenMode) {
            frameLayoutStepList.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) binding.playerView.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            binding.playerView.setLayoutParams(params);
            binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);
            if (actionBar != null) {
                actionBar.hide();
            }
        } else {
            frameLayoutStepList.setVisibility(View.VISIBLE);
            divider.setVisibility(View.VISIBLE);
            binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            if (actionBar != null) {
                actionBar.show();
            }
        }
    }


    @Override
    public void setPLayerView(Object dataBinding, int orientation) {
        FragmentStepDetailBinding binding = (FragmentStepDetailBinding) dataBinding;
        if (fullScreenMode) {
            frameLayoutStepList.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) binding.playerView.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            binding.playerView.setLayoutParams(params);
            binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);

            if (actionBar != null) {
                actionBar.hide();
            }
        } else {
            frameLayoutStepList.setVisibility(View.VISIBLE);
            divider.setVisibility(View.VISIBLE);
            binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);

            if (actionBar != null) {
                actionBar.show();
            }
        }
    }
}
