package com.fangio.edwardr.bakingapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.fangio.edwardr.bakingapp.databinding.CardRecipeBinding;
import com.fangio.edwardr.bakingapp.holders.RecipeViewHolder;
import com.fangio.edwardr.bakingapp.interfaces.OnItemClickListener;
import com.fangio.edwardr.bakingapp.models.Recipe;
import java.util.ArrayList;

/**
 * Created by edwardr on 12/4/17.
 */

public class RecipesAdapter extends RecyclerView.Adapter<RecipeViewHolder> {

    private ArrayList<Recipe> recipes;
    private OnItemClickListener mOnItemClickListener;

    public RecipesAdapter(ArrayList<Recipe> recipes, OnItemClickListener listener) {
        this.recipes = recipes;
        this.mOnItemClickListener = listener;
    }
    @Override
    public RecipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        CardRecipeBinding cardRecipeBinding = CardRecipeBinding.inflate(LayoutInflater.from(context), parent, false);
        return new RecipeViewHolder(cardRecipeBinding);
    }

    @Override
    public void onBindViewHolder(RecipeViewHolder holder, int position) {
        final Recipe recipe = recipes.get(position);
        holder.bind(recipe, mOnItemClickListener);

    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }
}
