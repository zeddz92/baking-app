package com.fangio.edwardr.bakingapp.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.bumptech.glide.Glide;
import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.databinding.FragmentStepDetailBinding;
import com.fangio.edwardr.bakingapp.holders.RecipeViewHolder;
import com.fangio.edwardr.bakingapp.interfaces.PlayerModeCallback;
import com.fangio.edwardr.bakingapp.interfaces.OnNextPrevStep;
import com.fangio.edwardr.bakingapp.models.Recipe;
import com.fangio.edwardr.bakingapp.models.Step;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import static com.fangio.edwardr.bakingapp.fragments.StepListFragment.ARG_RECIPE;

public class StepDetailFragment extends Fragment implements ExoPlayer.EventListener {
    private FragmentStepDetailBinding mBinding;
    private SimpleExoPlayer mExoPlayer;
    private Recipe mRecipe;
    private Step mStep;
    private OnNextPrevStep mOnNextPrevStep;
    private ProgressBar mPlayerProgressBar;
    private LinearLayout mLinearLayoutPlayControl;
    private long mLastPosition;
    private PlayerModeCallback mPlayerMode;
    private boolean mInErrorState;

    public static final String ARG_STEP_POSITION = "stepPos";


    public StepDetailFragment() {
    }


    public static StepDetailFragment newInstance(Recipe recipe, int stepPosition) {
        StepDetailFragment fragment = new StepDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_RECIPE, recipe);
        args.putInt(ARG_STEP_POSITION, stepPosition);
        fragment.setArguments(args);
        return fragment;
    }


    public FragmentStepDetailBinding getBinding() {
        return mBinding;
    }


    private void initializePlayer() {
        if (mExoPlayer == null && mStep != null && !TextUtils.isEmpty(mStep.getVideoURL())) {

            Uri mediaUri = Uri.parse(mStep.getVideoURL());
            TrackSelector trackSelector = new DefaultTrackSelector();
            LoadControl loadControl = new DefaultLoadControl();
            mExoPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector, loadControl);
            mBinding.playerView.setPlayer(mExoPlayer);
            mExoPlayer.addListener(this);
            String userAgent = Util.getUserAgent(getContext(), "BakingApp");
            MediaSource mediaSource = new ExtractorMediaSource(mediaUri, new DefaultDataSourceFactory(
                    getContext(), userAgent), new DefaultExtractorsFactory(), null, null);
            boolean haveResumePosition = mLastPosition != C.INDEX_UNSET;
            if (haveResumePosition) {
                mExoPlayer.seekTo(mLastPosition);
            }
            mExoPlayer.prepare(mediaSource, !haveResumePosition, false);
            mInErrorState = false;
            mExoPlayer.setPlayWhenReady(true);
        }
    }


    private void releasePlayer() {
        if (mExoPlayer != null) {
            mExoPlayer.stop();
            updateResumePosition();
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }


    private void updateResumePosition() {
        mLastPosition = mExoPlayer.getCurrentPosition();
    }


    private void clearResumePosition() {
        mLastPosition = C.TIME_UNSET;
    }


    private static boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                mRecipe = getArguments().getParcelable(ARG_RECIPE);
                int position = getArguments().getInt(ARG_STEP_POSITION);
                mStep = mRecipe.getSteps().get(position);
            }
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(getString(R.string.instance_step_key), mStep);
        outState.putParcelable(getString(R.string.instance_recipe_key), mRecipe);
        if (mExoPlayer != null) {
            mLastPosition = mExoPlayer.getCurrentPosition();
        }
        outState.putLong(getString(R.string.instance_player_resume_position_key), mLastPosition);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mOnNextPrevStep = (OnNextPrevStep) context;
            mPlayerMode = (PlayerModeCallback) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnNextPrevStep");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_step_detail, container, false);
        mPlayerProgressBar = mBinding.getRoot().findViewById(R.id.exo_progress_bar);
        mLinearLayoutPlayControl = mBinding.getRoot().findViewById(R.id.exo_play_control);
        ImageButton imageButtonExpandPlayer = mBinding.getRoot().findViewById(R.id.image_button_expand);
        mPlayerProgressBar.setVisibility(View.VISIBLE);
        imageButtonExpandPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnNextPrevStep.onFullScreenButtonClick(mBinding);
            }
        });

        clearResumePosition();

        if (savedInstanceState != null
                && savedInstanceState.containsKey(getString(R.string.instance_step_key))
                && savedInstanceState.containsKey(getString(R.string.instance_recipe_key))) {
            mStep = savedInstanceState.getParcelable(getString(R.string.instance_step_key));
            mRecipe = savedInstanceState.getParcelable(getString(R.string.instance_recipe_key));
            mLastPosition = savedInstanceState.getLong(getString(R.string.instance_player_resume_position_key));
        }

        if (mBinding.textViewInstruction != null) {
            mBinding.textViewInstruction.setText(mStep.getDescription());
            mBinding.textViewShortDescription.setText(mStep.getShortDescription());
            mBinding.textViewRecipeName.setText(mRecipe.getName());
            mBinding.imageViewRecipeImage.setImageDrawable(mBinding.getRoot().getContext().getResources().getDrawable(RecipeViewHolder.DEFAULT_RECIPE_IMAGE));

            if (!TextUtils.isEmpty(mRecipe.getImage())) {
                    Glide.with(this).load(mRecipe.getImage()).into(mBinding.imageViewRecipeImage);
            }
            if (TextUtils.isEmpty(mStep.getVideoURL())) {
                mBinding.textViewPlayerError.setVisibility(View.VISIBLE);
                mBinding.playerView.setContentDescription(getString(R.string.error_player));
            }
            mBinding.buttonNextStep.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnNextPrevStep.onNextButtonClick();
                }
            });
            mBinding.buttonPrevStep.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnNextPrevStep.onPrevButtonClick();
                }
            });
        }
        mPlayerMode.setPLayerView(mBinding, getResources().getConfiguration().orientation );
        return mBinding.getRoot();
    }


    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23 && mExoPlayer == null) {
            initializePlayer();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (Util.SDK_INT <= 23 && mExoPlayer == null) {
            initializePlayer();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23 && mExoPlayer != null) {
            mLastPosition = mExoPlayer.getCurrentPosition();
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23 && mExoPlayer != null) {
            mLastPosition = mExoPlayer.getCurrentPosition();
            releasePlayer();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }


    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {
    }


    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
    }


    @Override
    public void onLoadingChanged(boolean isLoading) {
    }


    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == ExoPlayer.STATE_READY) {
            mPlayerProgressBar.setVisibility(View.GONE);
            mLinearLayoutPlayControl.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onPlayerError(ExoPlaybackException error) {
        mInErrorState = true;
        if (isBehindLiveWindow(error)) {
            clearResumePosition();
            initializePlayer();
        } else {
            updateResumePosition();
        }
    }


    @Override
    public void onPositionDiscontinuity() {
        if (mInErrorState) {
            mPlayerProgressBar.setVisibility(View.GONE);
            updateResumePosition();
        }
    }
}
