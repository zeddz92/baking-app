package com.fangio.edwardr.bakingapp.holders;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import com.bumptech.glide.Glide;
import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.databinding.CardRecipeBinding;
import com.fangio.edwardr.bakingapp.interfaces.OnItemClickListener;
import com.fangio.edwardr.bakingapp.models.Recipe;

public class RecipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private CardRecipeBinding mBinding;
    private OnItemClickListener mOnItemClickListener;
    public static int DEFAULT_RECIPE_IMAGE = R.drawable.plate4;

    public RecipeViewHolder(CardRecipeBinding binding) {
        super(binding.getRoot());
        mBinding = binding;

        mBinding.getRoot().setOnClickListener(this);
    }

    public void bind(Recipe recipe, OnItemClickListener listener) {
        mOnItemClickListener = listener;
        mBinding.textViewRecipeName.setText(recipe.getName());
        mBinding.imageViewRecipe.setImageDrawable( mBinding.getRoot().getContext().getResources().getDrawable(DEFAULT_RECIPE_IMAGE) );

        if(!TextUtils.isEmpty(recipe.getImage())) {
            Glide.with(mBinding.getRoot().getContext()).load(recipe.getImage()).into(mBinding.imageViewRecipe);
        }

    }

    @Override
    public void onClick(View view) {
        int position = getAdapterPosition();
        mOnItemClickListener.onItemClick(position);
    }
}
