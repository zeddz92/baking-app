package com.fangio.edwardr.bakingapp.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.adapters.StepsAdapter;
import com.fangio.edwardr.bakingapp.databinding.FragmentStepListBinding;
import com.fangio.edwardr.bakingapp.interfaces.OnItemClickListener;
import com.fangio.edwardr.bakingapp.models.Recipe;
import com.fangio.edwardr.bakingapp.models.Step;

public class StepListFragment extends Fragment implements OnItemClickListener {
    private Recipe mRecipe;
    private FragmentStepListBinding mBinding;
    private OnItemClickListener mOnItemClickListener;

    public static final String ARG_RECIPE = "recipe";


    public StepListFragment() {
    }


    public static StepListFragment newInstance(Recipe recipe) {
        StepListFragment stepListFragment = new StepListFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_RECIPE, recipe);
        stepListFragment.setArguments(args);
        return stepListFragment;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(getString(R.string.instance_recipe_key), mRecipe);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mOnItemClickListener = (OnItemClickListener) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnItemClickListener");
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                mRecipe = getArguments().getParcelable(ARG_RECIPE);
            }

            Step ingredientsItem = new Step(-1, "Ingredients", "Ingredients", "", "");
            if (mRecipe != null)
                mRecipe.getSteps().add(0, ingredientsItem);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_step_list, container, false);

        if (savedInstanceState != null
                && savedInstanceState.containsKey(getString(R.string.instance_recipe_key))) {
            mRecipe = savedInstanceState.getParcelable(getString(R.string.instance_recipe_key));
        }
        if (mRecipe != null) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            mBinding.contentStepsRecycler.recyclerViewSteps.setLayoutManager(linearLayoutManager);
            mBinding.contentStepsRecycler.recyclerViewSteps.setHasFixedSize(true);

            StepsAdapter adapter = new StepsAdapter(mRecipe.getSteps(), this);
            mBinding.contentStepsRecycler.recyclerViewSteps.setAdapter(adapter);

        }

        return mBinding.getRoot();
    }


    @Override
    public void onItemClick(int clickedItemIndex) {

        mOnItemClickListener.onItemClick(clickedItemIndex);
    }
}
