package com.fangio.edwardr.bakingapp.models;

import java.util.ArrayList;

/**
 * Created by edwardr on 12/13/17.
 */

public class FakeRecipe {

    public static Recipe getRecipe() {
        Recipe recipe = new Recipe();

        recipe.setId(1);
        recipe.setName("Nutella Pie");
        recipe.setImage("");
        recipe.setServings(8);

        recipe.setSteps(getFakeSteps());
        recipe.setIngredients(getFakeIngredients());
        return recipe;
    }

    public static ArrayList<Ingredient> getFakeIngredients() {
        ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();

        Ingredient ingredient = new Ingredient();
        ingredient.setQuantity(2.0);
        ingredient.setMeasure("CUP");
        ingredient.setIngredient("Graham Cracker crumbs");
        ingredients.add(ingredient);

        ingredient = new Ingredient();
        ingredient.setQuantity(6.0);
        ingredient.setMeasure("TBLSP");
        ingredient.setIngredient("unsalted butter, melted");
        ingredients.add(ingredient);

        return ingredients;


    }

    public static ArrayList<Step> getFakeSteps() {
        ArrayList<Step> steps = new ArrayList<Step>();

        Step step = new Step();
        step.setId(0);
        step.setShortDescription("Recipe Introduction");
        step.setVideoURL("https://d17h27t6h515a5.cloudfront.net/topher/2017/April/58ffd974_-intro-creampie/-intro-creampie.mp4");
        step.setDescription("Recipe Introduction");
        step.setThumbnailURL("");
        steps.add(step);

        step = new Step();
        step.setId(1);
        step.setShortDescription("Starting prep");
        step.setVideoURL("");
        step.setThumbnailURL("");
        step.setDescription("1. Preheat the oven to 350°F. Butter a 9\" deep dish pie pan.");
        steps.add(step);

        step = new Step();
        step.setId(1);
        step.setShortDescription("Melt butter and bittersweet chocolate.");
        step.setVideoURL("https://d17h27t6h515a5.cloudfront.net/topher/2017/April/58ffdc43_1-melt-choclate-chips-and-butter-brownies/1-melt-choclate-chips-and-butter-brownies.mp4");
        step.setThumbnailURL("");
        step.setDescription("2. Melt the butter and bittersweet chocolate together in a microwave or a double boiler. If microwaving, heat for 30 seconds at a time, removing bowl and stirring ingredients in between.");
        steps.add(step);

        return steps;


    }
}
