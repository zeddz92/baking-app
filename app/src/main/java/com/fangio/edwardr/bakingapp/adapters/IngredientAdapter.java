package com.fangio.edwardr.bakingapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.fangio.edwardr.bakingapp.databinding.CardIngredientBinding;
import com.fangio.edwardr.bakingapp.holders.IngredientViewHolder;
import com.fangio.edwardr.bakingapp.models.Ingredient;
import java.util.ArrayList;

/**
 * Created by edwardr on 12/5/17.
 */

public class IngredientAdapter extends RecyclerView.Adapter<IngredientViewHolder> {
    private ArrayList<Ingredient> ingredients;

    public IngredientAdapter(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;

    }
    @Override
    public IngredientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        CardIngredientBinding cardIngredientBinding = CardIngredientBinding.inflate(LayoutInflater.from(context), parent, false);

        return new IngredientViewHolder(cardIngredientBinding);
    }

    @Override
    public void onBindViewHolder(IngredientViewHolder holder, int position) {
        final Ingredient ingredient = ingredients.get(position);
        holder.bind(ingredient);
    }

    @Override
    public int getItemCount() {
        return ingredients.size();
    }
}
