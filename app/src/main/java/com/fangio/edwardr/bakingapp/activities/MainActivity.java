package com.fangio.edwardr.bakingapp.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.Snackbar;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.fangio.edwardr.bakingapp.IdlingResource.SimpleIdlingResource;
import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.adapters.RecipesAdapter;
import com.fangio.edwardr.bakingapp.databinding.ActivityMainBinding;
import com.fangio.edwardr.bakingapp.interfaces.OnItemClickListener;
import com.fangio.edwardr.bakingapp.interfaces.OnResponseListener;
import com.fangio.edwardr.bakingapp.models.Recipe;
import com.fangio.edwardr.bakingapp.utils.NetworkUtils;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnItemClickListener {

    private ActivityMainBinding mBinding;
    private ArrayList<Recipe> mRecipes = new ArrayList<Recipe>();
    private RecipesAdapter mAdapter;

    @Nullable
    private SimpleIdlingResource mIdlingResource;

    @VisibleForTesting
    @NonNull
    public IdlingResource getIdlingResource() {
        if (mIdlingResource == null) {
            mIdlingResource = new SimpleIdlingResource();
        }
        return mIdlingResource;
    }


    private void fetchRecipesRequest() {
        NetworkUtils.getInstance().fetchRecipeData(
                new OnResponseListener<ArrayList<Recipe>>() {
                    @Override
                    public void onResponse(ArrayList<Recipe> result) {
                        mBinding.progressBar.setVisibility(View.GONE);
                        mRecipes.addAll(result);
                        mAdapter.notifyDataSetChanged();
                        if (mIdlingResource != null) {
                            mIdlingResource.setIdleState(true);
                        }
                    }

                    @Override
                    public void onError(String error) {
                        mBinding.progressBar.setVisibility(View.GONE);
                        Snackbar snackbar = Snackbar.make(mBinding.coordinatorLayout, error, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                }
        );
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(getString(R.string.instance_recipes_key), mRecipes);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int columnNumber = getResources().getInteger(R.integer.grid_columns);
        NetworkUtils.getInstance(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, columnNumber, GridLayoutManager.VERTICAL, false);
        mBinding.contentRecipesRecycler.recyclerViewRecipes.setLayoutManager(gridLayoutManager);
        mBinding.contentRecipesRecycler.recyclerViewRecipes.setHasFixedSize(true);
        mAdapter = new RecipesAdapter(mRecipes, this);
        mBinding.contentRecipesRecycler.recyclerViewRecipes.setAdapter(mAdapter);

        if (savedInstanceState == null) {
            fetchRecipesRequest();
        }

        getIdlingResource();
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(getString(R.string.instance_recipes_key))) {
            ArrayList<Recipe> instanceRecipes = savedInstanceState.getParcelableArrayList(getString(R.string.instance_recipes_key));
            mRecipes.addAll(instanceRecipes);
            mAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onItemClick(int clickedItemIndex) {

        Recipe recipe = mRecipes.get(clickedItemIndex);

        Intent intent = new Intent(this, RecipeDetailActivity.class);
        intent.putExtra(getString(R.string.intent_recipe_key), recipe);
        startActivity(intent);
    }
}
