package com.fangio.edwardr.bakingapp.interfaces;

/**
 * Created by edwardr on 12/4/17.
 */

public interface OnResponseListener<T> {
    public void onResponse(T result);
    public void onError(String error);
}
