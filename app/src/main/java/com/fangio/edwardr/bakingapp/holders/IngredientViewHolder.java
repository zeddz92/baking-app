package com.fangio.edwardr.bakingapp.holders;

import android.support.v7.widget.RecyclerView;
import com.fangio.edwardr.bakingapp.databinding.CardIngredientBinding;
import com.fangio.edwardr.bakingapp.models.Ingredient;

public class IngredientViewHolder extends RecyclerView.ViewHolder {
    private CardIngredientBinding mBinding;

    public IngredientViewHolder(CardIngredientBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bind(Ingredient ingredient) {
        mBinding.textViewIngredientName.setText( ingredient.getIngredient());
        mBinding.textViewIngredientQuantity.setText(String.valueOf(ingredient.getQuantity()));
        mBinding.textViewIngredientMeasure.setText(ingredient.getMeasure());
    }
}
