package com.fangio.edwardr.bakingapp.interfaces;

/**
 * Created by edwardr on 12/5/17.
 */

public interface OnNextPrevStep<T> {
    void onNextButtonClick();
    void onPrevButtonClick();
    void onFullScreenButtonClick(T binding);

}
