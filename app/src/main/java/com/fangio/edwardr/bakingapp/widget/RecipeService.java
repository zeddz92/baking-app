package com.fangio.edwardr.bakingapp.widget;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.interfaces.OnResponseListener;
import com.fangio.edwardr.bakingapp.models.Ingredient;
import com.fangio.edwardr.bakingapp.models.Recipe;
import com.fangio.edwardr.bakingapp.utils.NetworkUtils;

import java.util.ArrayList;

/**
 * Created by edwardr on 12/11/17.
 */

public class RecipeService extends IntentService {
    public static final String ACTION_WATER_PLANT = "getPlants";
    public static final String ACTION_UPDATE_RECIPE_WIDGETS ="com.fangio.edwardr.bakingapp.action.update.recipe";
    public static ArrayList<Recipe> recipes;
    public static ArrayList<Ingredient> ingredients;


    public RecipeService(){
        super("RecipeService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_UPDATE_RECIPE_WIDGETS.equals(action)) {
                handleActionUpdateRecipeWidgets();
            }
        }
    }

    private void handleActionUpdateRecipeWidgets() {


        NetworkUtils.getInstance(this).fetchRecipeData(new OnResponseListener<ArrayList<Recipe>>() {
            @Override
            public void onResponse(ArrayList<Recipe> result) {
                recipes = new ArrayList<Recipe>();
                recipes.addAll(result);

                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getBaseContext());
                int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(getBaseContext(), WidgetProvider.class));
                appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.widgetCollectionList);

                WidgetProvider.updateRecipeWidgets(getBaseContext(), appWidgetManager, appWidgetIds);
            }

            @Override
            public void onError(String error) {
                Log.v("ERROR", error);

            }
        });
    }

    public static void startActionUpdateWidgets(Context context) {
        Intent intent = new Intent(context, RecipeService.class);
        intent.setAction(ACTION_UPDATE_RECIPE_WIDGETS);
        context.startService(intent);
    }
}
