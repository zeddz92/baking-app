package com.fangio.edwardr.bakingapp.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.RemoteViews;
import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.activities.WidgetConfigurationActivity;
import com.fangio.edwardr.bakingapp.models.Recipe;

import static android.content.Context.MODE_PRIVATE;


public class WidgetProvider extends AppWidgetProvider {

    @Override
    public void onReceive(Context context, Intent intent) {

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, WidgetProvider.class));
        WidgetProvider.updateRecipeWidgets(context, appWidgetManager, appWidgetIds);
        super.onReceive(context, intent);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preference_widget_configuration), MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        for (int appWidgetId : appWidgetIds) {
            editor.remove(String.valueOf(appWidgetId));
        }
        super.onDeleted(context, appWidgetIds);
    }


    @SuppressLint("NewApi")
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {

        RecipeService.startActionUpdateWidgets(context);
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    private static RemoteViews initViews(Context context,
                                         int widgetId) {

        RemoteViews mView = new RemoteViews(context.getPackageName(),
                R.layout.widget_provider_layout);

        Intent widgetConfigurationIntent = new Intent(context, WidgetConfigurationActivity.class);

        widgetConfigurationIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        widgetConfigurationIntent.setData(Uri.parse("tel:/" + (int) System.currentTimeMillis()));
        PendingIntent widgetConfPendingIntent = PendingIntent.getActivity(context, 0,
                widgetConfigurationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preference_widget_configuration), MODE_PRIVATE);

        int position = sharedPref.getInt(String.valueOf(widgetId), 0);

        Intent intent = new Intent(context, WidgetService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);

        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
        if(RecipeService.recipes != null) {
            Recipe recipe = RecipeService.recipes.get(position);
            String widgetTitle = context.getString(R.string.label_ingredients_for) + " " + recipe.getName();
            mView.setTextViewText(R.id.txvWidgetTitle, widgetTitle);
        }


        mView.setRemoteAdapter(widgetId, R.id.widgetCollectionList, intent);
        mView.setOnClickPendingIntent(R.id.widgetTopBar, widgetConfPendingIntent);


        return mView;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        RemoteViews mView;
        mView = initViews(context, appWidgetId);
        appWidgetManager.updateAppWidget(appWidgetId, mView);

    }

    public static void updateRecipeWidgets(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }
}
