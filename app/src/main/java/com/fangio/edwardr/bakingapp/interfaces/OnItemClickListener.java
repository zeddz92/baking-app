package com.fangio.edwardr.bakingapp.interfaces;

/**
 * Created by edwardr on 12/4/17.
 */

public interface OnItemClickListener<T> {
    void onItemClick(int clickedItemIndex);
}
