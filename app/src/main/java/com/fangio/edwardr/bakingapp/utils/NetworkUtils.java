package com.fangio.edwardr.bakingapp.utils;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fangio.edwardr.bakingapp.interfaces.OnResponseListener;
import com.fangio.edwardr.bakingapp.models.Recipe;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by edwardr on 12/4/17.
 */

public class NetworkUtils {

    private static NetworkUtils instance = null;
    private RequestQueue requestQueue;

    private static final String BASE_URL = "https://d17h27t6h515a5.cloudfront.net/topher/2017/May/59121517_baking/baking.json";

    private NetworkUtils(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized NetworkUtils getInstance(Context context) {
        if (null == instance)
            instance = new NetworkUtils(context);
        return instance;
    }

    public static synchronized NetworkUtils getInstance() {
        if (null == instance) {
            throw new IllegalStateException(NetworkUtils.class.getSimpleName() +
                    " is not initialized, call getInstance(...) first");
        }
        return instance;
    }

    private NetworkUtils() {
    }

    public void fetchRecipeData(final OnResponseListener<ArrayList<Recipe>> listener) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ArrayList<Recipe> recipes;
                        ObjectMapper mapper = new ObjectMapper();
                        try {
                            recipes = mapper.readValue(response, new TypeReference<ArrayList<Recipe>>() {
                            });
                            listener.onResponse(recipes);
                        } catch (IOException e) {
                            listener.onError(e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(error.getLocalizedMessage());
                    }
                });

        requestQueue.add(stringRequest);
    }
}
