package com.fangio.edwardr.bakingapp.media;

import android.support.v4.media.session.MediaSessionCompat;

import com.google.android.exoplayer2.SimpleExoPlayer;

/**
 * Created by edwardr on 12/4/17.
 */

public class MySessionCallback extends MediaSessionCompat.Callback {

    private SimpleExoPlayer mExoPlayer;

    public MySessionCallback(SimpleExoPlayer exoPlayer) {
        this.mExoPlayer = exoPlayer;
    }

    @Override
    public void onPlay() {
        mExoPlayer.setPlayWhenReady(true);
    }

    @Override
    public void onPause() {
        mExoPlayer.setPlayWhenReady(false);
    }

    @Override
    public void onSkipToPrevious() {
        mExoPlayer.seekTo(0);
    }
}
