package com.fangio.edwardr.bakingapp.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.adapters.IngredientAdapter;
import com.fangio.edwardr.bakingapp.databinding.FragmentIngredientListBinding;
import com.fangio.edwardr.bakingapp.models.Ingredient;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class IngredientListFragment extends Fragment {

    private ArrayList<Ingredient> ingredients;
    private FragmentIngredientListBinding mBinding;
    private IngredientAdapter mAdapter;

    public IngredientListFragment() {
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(getString(R.string.instance_ingredients_key), ingredients);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_ingredient_list, container, false);

        if (savedInstanceState != null
                && savedInstanceState.containsKey(getString(R.string.instance_ingredients_key))) {
            ingredients = savedInstanceState.getParcelableArrayList(getString(R.string.instance_ingredients_key));
        }

        if (ingredients != null) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            mBinding.contentIngredientsRecycler.recyclerViewIngredients.setLayoutManager(linearLayoutManager);
            mBinding.contentIngredientsRecycler.recyclerViewIngredients.setHasFixedSize(true);
            mAdapter = new IngredientAdapter(ingredients);
            mBinding.contentIngredientsRecycler.recyclerViewIngredients.setAdapter(mAdapter);


        }

        // Inflate the layout for this fragment
        return mBinding.getRoot();
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    ;


}
