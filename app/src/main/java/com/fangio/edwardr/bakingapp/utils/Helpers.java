package com.fangio.edwardr.bakingapp.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by edwardr on 11/29/17.
 */

public class Helpers {
    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int scalingFactor = 180;
        int noOfColumns = (int) (dpWidth / scalingFactor);
        return noOfColumns;
    }

    public static int getSmallestScreenSize(Context context) {
        Configuration configuration = context.getResources().getConfiguration();

        return configuration.smallestScreenWidthDp;
    }


}
