package com.fangio.edwardr.bakingapp.activities;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.adapters.RecipesAdapter;
import com.fangio.edwardr.bakingapp.databinding.ActivityWidgetConfigurationBinding;
import com.fangio.edwardr.bakingapp.interfaces.OnItemClickListener;
import com.fangio.edwardr.bakingapp.interfaces.OnResponseListener;
import com.fangio.edwardr.bakingapp.models.Recipe;
import com.fangio.edwardr.bakingapp.utils.NetworkUtils;
import com.fangio.edwardr.bakingapp.widget.RecipeService;
import com.fangio.edwardr.bakingapp.widget.WidgetProvider;

import java.util.ArrayList;

public class WidgetConfigurationActivity extends AppCompatActivity implements OnItemClickListener {
    private int mAppWidgetId = 0;
    private ArrayList<Recipe> recipes = new ArrayList<Recipe>();
    private RecipesAdapter mAdapter;
    private ActivityWidgetConfigurationBinding mBinding;


    private void fetchRecipesRequest() {
        NetworkUtils.getInstance(this).fetchRecipeData(
                new OnResponseListener<ArrayList<Recipe>>() {
                    @Override
                    public void onResponse(ArrayList<Recipe> result) {
                        mBinding.progressBar.setVisibility(View.GONE);
                        recipes.addAll(result);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(String error) {
                        mBinding.progressBar.setVisibility(View.GONE);
                        Toast.makeText(WidgetConfigurationActivity.this, error, Toast.LENGTH_LONG).show();
                    }
                }
        );
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(getString(R.string.instance_recipes_key), recipes);
        outState.putInt(getString(R.string.instance_widget_id_key), mAppWidgetId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget_configuration);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_widget_configuration);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null) {

            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        int columnNumber = getResources().getInteger(R.integer.grid_columns);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, columnNumber, GridLayoutManager.VERTICAL, false);
        mBinding.contentRecipesRecycler.recyclerViewRecipes.setLayoutManager(gridLayoutManager);
        mBinding.contentRecipesRecycler.recyclerViewRecipes.setHasFixedSize(true);
        mAdapter = new RecipesAdapter(recipes, this);
        mBinding.contentRecipesRecycler.recyclerViewRecipes.setAdapter(mAdapter);

        if (savedInstanceState == null) {
            fetchRecipesRequest();
        }

    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(getString(R.string.instance_recipes_key))
                && savedInstanceState.containsKey(getString(R.string.instance_widget_id_key))) {
            ArrayList<Recipe> instanceRecipes = savedInstanceState.getParcelableArrayList(getString(R.string.instance_recipes_key));
            recipes.addAll(instanceRecipes);
            mAppWidgetId = savedInstanceState.getInt(getString(R.string.instance_widget_id_key));
            mAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onItemClick(int clickedItemIndex) {

        Recipe recipe = recipes.get(clickedItemIndex);
        Intent intent = new Intent(getBaseContext(), WidgetConfigurationActivity.class);

        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preference_widget_configuration), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.widget_configuration_recipe_position), clickedItemIndex);
        editor.putInt(String.valueOf(mAppWidgetId), clickedItemIndex);
        editor.apply();

        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        intent.setData(Uri.parse("tel:/" + (int) System.currentTimeMillis()));
        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getBaseContext());

        RecipeService.recipes = recipes;
        RecipeService.ingredients = recipe.getIngredients();

        RemoteViews views = new RemoteViews(getBaseContext().getPackageName(), R.layout.widget_provider_layout);

        String widgetTitle = getString(R.string.label_ingredients_for) + " " + recipe.getName();
        views.setTextViewText(R.id.txvWidgetTitle, widgetTitle);
        views.setOnClickPendingIntent(R.id.widgetTopBar, pendingIntent);
        appWidgetManager.updateAppWidget(mAppWidgetId, views);

        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        setResult(RESULT_OK, resultValue);
        appWidgetManager.notifyAppWidgetViewDataChanged(mAppWidgetId, R.id.widgetCollectionList);
        finish();
    }
}
