package com.fangio.edwardr.bakingapp.activities;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.fragments.IngredientListFragment;
import com.fangio.edwardr.bakingapp.models.Ingredient;
import java.util.ArrayList;

public class IngredientActivity extends AppCompatActivity {
    private ArrayList<Ingredient> mIngredients;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String INGREDIENTS_KEY = getString(R.string.intent_recipe_ingredients_key);
        setContentView(R.layout.activity_ingredient);
        IngredientListFragment ingredientListFragment = new IngredientListFragment();
        ActionBar actionBar = getSupportActionBar();

        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.toolbar_title_ingredients));
        }

        Intent data= getIntent();
        if(data.hasExtra(INGREDIENTS_KEY)) {
            mIngredients = data.getParcelableArrayListExtra(INGREDIENTS_KEY);
        }

        if(savedInstanceState == null) {
            ingredientListFragment.setIngredients(mIngredients);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .add(R.id.ingredients_container, ingredientListFragment).commit();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
