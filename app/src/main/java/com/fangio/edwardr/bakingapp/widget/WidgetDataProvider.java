package com.fangio.edwardr.bakingapp.widget;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService.RemoteViewsFactory;

import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.interfaces.OnResponseListener;
import com.fangio.edwardr.bakingapp.models.Ingredient;
import com.fangio.edwardr.bakingapp.models.Recipe;
import com.fangio.edwardr.bakingapp.utils.NetworkUtils;

@SuppressLint("NewApi")
public class WidgetDataProvider implements RemoteViewsFactory {

    private List<String> mCollections = new ArrayList<String>();
    private int selecteRecipePos = 0;

    private Context mContext = null;
    private ArrayList<Recipe> recipes = new ArrayList<Recipe>();
    private ArrayList<Ingredient> ingredients =new ArrayList<Ingredient>();
    private int widgetId = 0;

    public WidgetDataProvider(Context context, Intent intent) {
        mContext = context;
        SharedPreferences sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.preference_widget_configuration), Context.MODE_PRIVATE);
        widgetId = intent.getExtras().getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, 0);
        selecteRecipePos = sharedPref.getInt(String.valueOf(widgetId), 0);

        Log.v("WidgetDataProvider", String.valueOf(widgetId));
    }

    @Override
    public int getCount() {
        return ingredients.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews mView = new RemoteViews(mContext.getPackageName(),
                android.R.layout.simple_list_item_1);
        mView.setTextViewText(android.R.id.text1, ingredients.get(position).getIngredient());
        String det = String.valueOf(ingredients.get(position).getQuantity()) + " " + ingredients.get(position).getMeasure();
        mView.setTextViewText(android.R.id.text2, det);

        mView.setTextColor(android.R.id.text1, Color.BLACK);

        return mView;
    }


    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onCreate() {
        initData();
    }

    @Override
    public void onDataSetChanged() {
        SharedPreferences sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.preference_widget_configuration), Context.MODE_PRIVATE);
        selecteRecipePos = sharedPref.getInt(String.valueOf(widgetId), 0);
        initData();
       /* NetworkUtils.getInstance(mContext).fetchRecipeData(new OnResponseListener<ArrayList<Recipe>>() {
            @Override
            public void onResponse(ArrayList<Recipe> result) {
                SharedPreferences sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.preference_widget_configuration), Context.MODE_PRIVATE);
                selecteRecipePos = sharedPref.getInt(mContext.getString(R.string.widget_configuration_recipe_position), 0);
                Log.v("POSITIONCONFI", String.valueOf(selecteRecipePos));

                RecipeService.recipes = result;
                RecipeService.ingredients = result.get(selecteRecipePos).getIngredients();
                initData();
            }

            @Override
            public void onError(String error) {

            }
        });*/

    }

    private void initData() {

        if (RecipeService.recipes != null) {
            recipes = (ArrayList<Recipe>) RecipeService.recipes.clone();
            ingredients = recipes.get(selecteRecipePos).getIngredients();
        }

    }

    @Override
    public void onDestroy() {

    }

}
