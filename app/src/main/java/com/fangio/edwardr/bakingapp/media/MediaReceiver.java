package com.fangio.edwardr.bakingapp.media;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;

/**
 * Created by edwardr on 12/4/17.
 */

public class MediaReceiver extends BroadcastReceiver {
    private MediaSessionCompat mMediaSession;

    public MediaReceiver(MediaSessionCompat mediaSession) {
        this.mMediaSession = mediaSession;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        MediaButtonReceiver.handleIntent(mMediaSession, intent);

    }
}
