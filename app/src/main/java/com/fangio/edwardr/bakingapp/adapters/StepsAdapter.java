package com.fangio.edwardr.bakingapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.fangio.edwardr.bakingapp.databinding.CardStepBinding;
import com.fangio.edwardr.bakingapp.holders.StepViewHolder;
import com.fangio.edwardr.bakingapp.interfaces.OnItemClickListener;
import com.fangio.edwardr.bakingapp.models.Step;
import java.util.ArrayList;

/**
 * Created by edwardr on 12/4/17.
 */

public class StepsAdapter extends RecyclerView.Adapter<StepViewHolder> {

    private ArrayList<Step> steps;
    private OnItemClickListener mOnItemClickListener;

    public StepsAdapter(ArrayList<Step> steps,  OnItemClickListener listener) {
        this.steps = steps;
        this.mOnItemClickListener = listener;
    }

    @Override
    public StepViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        CardStepBinding cardStepBinding = CardStepBinding.inflate(LayoutInflater.from(context), parent, false);

        return new StepViewHolder(cardStepBinding);
    }

    @Override
    public void onBindViewHolder(StepViewHolder holder, int position) {
        final Step step = steps.get(position);
        holder.bind(step, mOnItemClickListener);
    }

    @Override
    public int getItemCount() {
        return steps.size();
    }
}
