package com.fangio.edwardr.bakingapp.holders;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.databinding.CardStepBinding;
import com.fangio.edwardr.bakingapp.interfaces.OnItemClickListener;
import com.fangio.edwardr.bakingapp.models.Step;

public class StepViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private CardStepBinding mBinding;
    private OnItemClickListener mOnItemClickListener;

    public StepViewHolder(CardStepBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
        mBinding.getRoot().setOnClickListener(this);
    }

    public void bind(Step step, OnItemClickListener listener) {
        int position = getAdapterPosition();
        mBinding.textViewStepDescription.setText(step.getShortDescription());
        mOnItemClickListener = listener;
        if (position == 0) {
            mBinding.imageViewStepThumb.setImageDrawable(mBinding.getRoot().getContext().getResources().getDrawable(R.drawable.ic_harvest));
            mBinding.imageViewStepThumb.setContentDescription(mBinding.getRoot().getContext().getString(R.string.content_description_recipe_ingredients_thumbnail));
        } else {
            setStepImage(step.getThumbnailURL());

        }
    }

    private void setStepImage(String thumbUrl) {
        if (!TextUtils.isEmpty(thumbUrl)) {
            Glide.with(itemView.getContext()).load(thumbUrl)
                    .listener(new RequestListener<String, GlideDrawable>() {
                                  @Override
                                  public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                      mBinding.imageViewStepThumb.setImageDrawable(mBinding.getRoot().getContext().getResources().getDrawable(R.drawable.ic_mixer));
                                      return true;
                                  }

                                  @Override
                                  public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                      return false;
                                  }
                              }
                    )
                    .into(mBinding.imageViewStepThumb);
        }
    }

    @Override
    public void onClick(View view) {
        int position = getAdapterPosition();
        mOnItemClickListener.onItemClick(position);
    }
}
