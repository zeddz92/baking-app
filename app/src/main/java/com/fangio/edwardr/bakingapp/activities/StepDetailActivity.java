package com.fangio.edwardr.bakingapp.activities;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.fangio.edwardr.bakingapp.R;
import com.fangio.edwardr.bakingapp.fragments.StepDetailFragment;
import com.fangio.edwardr.bakingapp.interfaces.PlayerModeCallback;
import com.fangio.edwardr.bakingapp.interfaces.OnNextPrevStep;
import com.fangio.edwardr.bakingapp.models.Recipe;
import com.fangio.edwardr.bakingapp.models.Step;
import java.util.ArrayList;

public class StepDetailActivity extends AppCompatActivity implements OnNextPrevStep, PlayerModeCallback {

    private int currentStepPosition = 0;
    private ArrayList<Step> steps = new ArrayList<Step>();
    private Recipe mRecipe;
    private boolean fullScreenMode = false;


    private void setStepDetailFragment(int stepPosition) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(R.id.step_detail_container, StepDetailFragment.newInstance(mRecipe, stepPosition)).commit();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_detail);

        if(getIntent().hasExtra(getString(R.string.intent_recipe_key))
                && getIntent().hasExtra(getString(R.string.intent_recipe_step_position_key))) {
            mRecipe = getIntent().getParcelableExtra(getString(R.string.intent_recipe_key));
            steps = mRecipe.getSteps();
            currentStepPosition = getIntent().getIntExtra(getString(R.string.intent_recipe_step_position_key), 0);

            if(savedInstanceState == null) {
                FragmentManager fragmentManager = getSupportFragmentManager();

                fragmentManager.beginTransaction()
                        .add(R.id.step_detail_container, StepDetailFragment.newInstance(mRecipe, currentStepPosition)).commit();
            }

        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (android.provider.Settings.System.getInt(getContentResolver(),
                Settings.System.ACCELEROMETER_ROTATION, 0) == 1){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        }
    }


    @Override
    public void onNextButtonClick() {
        int newPos = currentStepPosition + 1;
        if(newPos < steps.size()) {
            setStepDetailFragment(newPos);
            currentStepPosition++;
        }
    }


    @Override
    public void onPrevButtonClick() {
        int newPos = currentStepPosition - 1;
        if(newPos >= 0) {
            setStepDetailFragment(newPos);
            currentStepPosition--;
        }
    }


    @Override
    public void onFullScreenButtonClick(Object dataBinding) {
        fullScreenMode = !fullScreenMode;

        int currentOrientation = getResources().getConfiguration().orientation;
        if(currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

    }


    @Override
    public void setPLayerView(Object dataBinding, int orientation) {


    }
}
